package main

import (
	"flag"
	"io/ioutil"
	"os"
	"path"
	"fmt"
	"strings"
)


func listDir( dirName string ) []os.FileInfo {
	dirContents, errMsg := ioutil.ReadDir( dirName )
	if errMsg != nil {
		return nil
	}

	return dirContents
}

func main() {
	baseDir := flag.String( "dir", "", "Specify Base Directory To Search")
	flag.Parse()
	for _, indivResult := range listDir( *baseDir ) {
		if indivResult.IsDir() && !strings.HasPrefix( indivResult.Name(), "." ) {
			fmt.Println( "Searching Dir", indivResult.Name() )
			for _, castDir := range listDir( path.Join( *baseDir, indivResult.Name() ) ) {
				if !strings.HasPrefix( castDir.Name(), "." ) && !castDir.IsDir() {
					fmt.Println( path.Join( path.Join( *baseDir, indivResult.Name() ), castDir.Name() ) )
				}
			}
		}
	}
}
