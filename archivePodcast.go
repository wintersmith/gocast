package main

import (
	"flag"
	"fmt"
	"github.com/mmcdole/gofeed"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strings"
)

func main() {
	var dirName string
	flag.StringVar(&dirName, "dir", "", "Directory In Which To Podcast Shows Are Kept")
	rssFeed := flag.String("url", "", "URL Of RSS Feed For Show")
	showName := flag.String("show", "", "Name Of Show")
	curUser, _ := user.Current()
	homeDir := curUser.HomeDir
	flag.Parse()

	if dirName != "" && rssFeed != nil && showName != nil {
		var dirPath string
		fmt.Println("Downloading ", *showName)
		fmt.Println("From ", *rssFeed)
		fmt.Println("To ", dirName)
		if dirName == "~" {
			dirPath = homeDir
		} else if strings.HasPrefix(dirName, "~/") {
			dirPath = path.Join(homeDir, dirName[2:])
		}

		getPodcast(*rssFeed, dirPath, *showName)
	}

	os.Exit(0)
}
func getPodcast(rssUrl string, destDir string, feedName string) {
	feedParser := gofeed.NewParser()
	rssFeed, _ := feedParser.ParseURL(rssUrl)
	for _, indivEntry := range rssFeed.Items {
		var epSeason string
		if indivEntry.ITunesExt.Season == "" {
			epSeason = "0"
		} else {
			epSeason = indivEntry.ITunesExt.Season
		}
		if prepareDir(path.Join(path.Join(destDir, feedName), epSeason)) {
			for _, iFile := range indivEntry.Enclosures {
				parsedUrl, errMsg := url.Parse(iFile.URL)
				if errMsg != nil {
					panic(errMsg)
				}
				fName := filepath.Base(parsedUrl.Path)
				downloadFile(iFile.URL, path.Join(path.Join(path.Join(destDir, feedName), epSeason), fName))
			}
		}
	}

}
func prepareDir(dirName string) bool {
	if _, errMsg := os.Stat(dirName); os.IsNotExist(errMsg) {
		fmt.Println("Creating ", dirName)
		err := os.MkdirAll(dirName, 0755)
		if err != nil {
			return false
		}
	}
	return true
}
func downloadFile(fileUrl string, fileDest string) error {
	httpResp, errMsg := http.Get(fileUrl)
	if errMsg != nil {
		return errMsg
	}
	defer httpResp.Body.Close()
	savedFile, errMsg := os.Create(fileDest)
	if errMsg != nil {
		return errMsg
	}
	_, errMsg = io.Copy(savedFile, httpResp.Body)

	return errMsg
}
